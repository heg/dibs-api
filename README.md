# Api du projet eRentes.ch

## pile technique

- Slim Framwork 4
- php-di 6
- Client Oauth: une implémentation de `League\OAuth2\Client\Provider\AbstractProvider`. Pour le moment nous utilisons `League\OAuth2\Client\Provider\GenericProvider`
- Http Client: `GuzzleHttp\Client`
- DB: une implémentation de `Predis\ClientInterface` => `Predis\Client`

## Installation

    cd ROOT_DIRECTORY
    cp .env.example .env
    composer install

Puis il faut remplir les informations manquantes dans le fichier .env

## lancement serveur de dev

    cd ROOT_DIRECTORY
    php -S 0.0.0.0:1338 -t public public/index.php

## Création d'une nouvelle version

    cd ROOT_DIRECTORY
    npm version --no-git-tag-version patch|minor|major

## Sessions

Pour l'instant, le gestionnaire de session est le gestionnaire par défaut de PHP, pour changer ça il suffit de changer l'injection de `SessionHandler` (dans `config/injections.php`) et d'utiliser un gestionnaire de session implémentant l'interface [SessionHandlerInterface](https://www.php.net/manual/fr/class.sessionhandlerinterface.php)

### Données stockées dans la session

- Les informations de l'access token pour l'authentification au service (SwissId, auth0, ...)

## Authentification

Les utilisateurs peuvent se connecter via des services d'authentification externe (auth0, SwissId ...), les informations nécessaires à la connection doivent être mise dans le fichier .env

## Routes

### Routes non authentifiées

- GET `/oauth/token` => permet à l'utilisateur de se connecter
- GET `/oauth/disconnect` => permet à l'utilisateur de se déconnecter

### Routes authentifiées

- GET `/me` => retourne les informations du resourceOwner
- GET `/states` => retourne l'état du compte utilisateur
- GET `/openpk/pension-funds` => retourne tous les fonds de pensions
- POST `/openpk/pension-funds` => Enregistre les fonds de pensions de l'utilisateur
```
    Attend un body json
    {
      "ids": [
        "ZH 9999",
        "NFR-906"
      ]
    }
```
- GET `/openpk/pension-funds` => Récupère le tableau des ids des fonds de pension de l'utilisateur
- GET `/openpk/pension-funds/token` => Permet de faire tout le processus de récupération des droits d'accès aux Apis OpenPK des différentes caisses de l'utilisateur
- GET `/openpk/pension-funds/policies` => Retourne un tableau de toutes les pensions de l'utilisateur