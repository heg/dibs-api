<?php

namespace Dibs\Api\User;

use Dibs\Api\Login\LoginManager;
use Dibs\Api\User\DbManager;

class UserManager
{
    protected $dbManager;
    protected $loginManager;
    protected $secretKey;

    public function __construct(DbManager $dbManager, LoginManager $loginManager)
    {
        $this->dbManager    = $dbManager;
        $this->loginManager = $loginManager;

        $this->dbManager->init($this->loginManager->getResourceOwner());
    }

    /**
     * Ajoute des valeurs dans la base utilisateur
     *
     * @param string  $path chemin du type folder.subfolder.subsubfolder ...
     * @param <mixed> $data données à enregistrer
     */
    public function appendToPrivateDb($path, $data)
    {
        $this->dbManager->set($path, $data);
    }

    /**
     * Récupère des données de la base utilisateur
     *
     * @param  string $path chemin du type folder.subfolder.subsubfolder ...
     * @return <mixed> données dans la base
     */
    public function retrieveFromPrivateDb($path)
    {
        return $this->dbManager->get($path);
    }
}
