<?php

namespace Dibs\Api\Data;

use Predis\ClientInterface as RedisClient;

class DataManager
{
    protected $redisClient;

    public function __construct(RedisClient $redisClient)
    {
        $this->redisClient = $redisClient;
    }

    public function get($key)
    {
        return $this->redisClient->get($key);
    }

    public function set($key, $value, $ttl = null)
    {

        if (!$ttl) {
            $this->redisClient->set($key, $value);
        } else {
            $this->redisClient->set($key, $value, 'EX', $ttl);
        }
    }

    public function touch($key, $ttl = null)
    {

        if (!$ttl) {
            return;
        }

        $this->redisClient->expire($key, $ttl);
    }
}
