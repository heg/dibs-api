<?php

namespace Dibs\Api\Login;

use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Provider\GenericResourceOwner;
use League\OAuth2\Client\Token\AccessToken;

class OAuthProvider extends GenericProvider
{
    protected $logoutUri;

    public function __construct($data)
    {
        $this->logoutUri = $data['logoutUri'];

        return parent::__construct($data);
    }

    public function getLogoutUri()
    {
        $protocol    = strpos(strtolower($_SERVER['INTERFACE_DOMAIN']), 'https') === false ? 'http' : 'https';
        $redirectUri = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/redirect';

        return $this->logoutUri . '&returnTo=' . urlencode($redirectUri);
    }

    /**
     * @inheritdoc
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        return new GenericResourceOwner($response, $_ENV['LOGIN_RESOURCE_OWNER_ID']);
    }
}
