<?php

namespace Dibs\Api\Login;

use Dibs\Api\Exceptions\UnauthorizedException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\App;
use Slim\Psr7\Response;

/**
 * Middleware vérifiant que l'utilisateur est connecté
 */
class IsAuthentifiedMiddleware
{
    protected $loginManager;
    protected $app;

    public function __construct(LoginManager $loginManager, App $app)
    {
        $this->loginManager = $loginManager;
        $this->app          = $app;
    }

    /**
     * Example middleware invokable class
     *
     * @param  ServerRequest  $request PSR-7 request
     * @param  RequestHandler $handler PSR-15 request handler
     * @return Response
     */
    public function __invoke(Request $request, RequestHandler $handler)
    {
        try {
            if (!$this->loginManager->isTokenValid($request)) {
                throw new UnauthorizedException('No valid token');
            }
        } catch (UnauthorizedException $e) {
            // si la connexion ne fonctionne pas et que la requête accêpte les contenus html, on redirige vers la page de connexion
            if (stristr($request->getHeaderLine('accept'), 'html')) {
                $newFlashUri = $request->getUri()->getPath() . '?' . $request->getUri()->getQuery();

                $routeParser = $this->app->getRouteCollector()->getRouteParser();
                $url         = $routeParser->urlFor('login', [], ['flash_uri' => $newFlashUri]);

                $response = $this->app->getResponseFactory()->createResponse();
                return $response
                    ->withHeader('Location', $url)
                    ->withStatus(302);
            }

            throw $e;
        }

        $response = $handler->handle($request);

        return $response;
    }
}
