<?php

namespace Dibs\Api\Synthesis;

class SimulationData
{
    public $employmentLevel;
    public $effectiveDate;
    public $declaredSalary;

    public function __construct($data)
    {
        $this->employmentLevel = $data['employmentLevel'];
        $this->effectiveDate   = $data['effectiveDate'];
        $this->declaredSalary  = $data['declaredSalary'];
    }
}
