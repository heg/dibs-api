<?php

namespace Dibs\Api\Synthesis;

use Dibs\Api\Open3K\Open3KManager;
use Dibs\Api\OpenAK\OpenAKManager;
use Dibs\Api\OpenPK\OpenPKManager;
use Dibs\Api\Synthesis\SimulationData;
use Dibs\Api\User\UserManager;

class SynthesisManager
{
    protected $openAKManager;
    protected $openPKManager;
    protected $open3KManager;
    protected $oAuthProviderFactory;

    public function __construct(Open3KManager $open3KManager, OpenPKManager $openPKManager, OpenAKManager $openAKManager, UserManager $userManager)
    {
        $this->openAKManager = $openAKManager;
        $this->openPKManager = $openPKManager;
        $this->open3KManager = $open3KManager;
        $this->userManager   = $userManager;
    }

    public function digest(SimulationData $simulation = null)
    {
        $digest = [
            'date'      => null,
            'annuities' => [
                '1_pillar' => 0,
                '2_pillar' => 0,
            ],
            'capitals'  => [
                '2_pillar'        => 0,
                '3_pillar'        => 0,
                'vested_benefits' => 0,
            ],
        ];

        $navs = $this->userManager->retrieveFromPrivateDb('openak.navs');
        $navs = trim($navs);
        try {
            $openAK = $this->openAKManager->retrievePensionCalculator($simulation);
        } catch (\Exception $e) {
        }

        $openPK = $this->openPKManager->getPolicies($simulation);
        $open3K = $this->open3KManager->getInfo();

        $digest['date'] = [
            'value' => $this->open3KManager->getPaydate(),
            'meta'  => [
                'description' => 'Valeur entrée lors du formulaire du 3ème pilier',
                'formula'     => '= open3K.payDate',
            ],
        ];
        $digest['annuities']['1_pillar'] = [
            // 'value' => end($openAK['originalPension'])['pension']['amount'],
            'value' => isset($openAK['originalPension']) ? array_column(end($openAK['originalPension'])['pensions'], 'amount', 'navs')[$navs] : 0,
            'meta'  => [
                'description' => 'Valeur de rente personnelle retournée par OpenAK',
                'formula'     => '= openAK.originalPension.pension.amount',
            ],
        ];

        $digest['annuities']['2_pillar'] = [
            'value' => array_reduce(
                $openPK,
                function ($sum, $pk) {
                    $pk       = json_decode(json_encode($pk), true);
                    $benefits = isset($pk['retirementCapital']) ? $pk['retirementCapital']['projectedRetirementBenefits'] : $pk['projectedRetirementBenefits'];
                    return $sum + end($benefits)['pension'];
                },
                0
            ),
            'meta'  => [
                'description' => 'Somme des rentes retournées par OpenPK',
                'formula'     => isset($pk['retirementCapital']) ? '= sum(openPK.retirementCapital.projectedRetirementBenefits.pension)' : '= sum(openPK.projectedRetirementBenefits.pension)',
                'variables'   => array_reduce(
                    $openPK,
                    function ($vars, $pk) {
                        $pk                                   = json_decode(json_encode($pk), true);
                        $benefits                             = isset($pk['retirementCapital']) ? $pk['retirementCapital']['projectedRetirementBenefits'] : $pk['projectedRetirementBenefits'];
                        $vars['rente ' . (sizeof($vars) + 1)] = end($benefits)['pension'];
                        return $vars;
                    },
                    []
                ),
            ],
        ];

        $digest['annuities']['total'] = [
            'value' => $digest['annuities']['1_pillar']["value"] + $digest['annuities']['2_pillar']["value"],
            'meta'  => [
                'description' => 'Somme des rentes',
                'formula'     => '= annuities.1_pillar + annuities+2_pillar',
            ],
        ];

        $digest['capitals']['2_pillar'] = [
            'value' => array_reduce(
                $openPK,
                function ($sum, $pk) {
                    $pk       = json_decode(json_encode($pk), true);
                    $benefits = isset($pk['retirementCapital']) ? $pk['retirementCapital']['projectedRetirementBenefits'] : $pk['projectedRetirementBenefits'];
                    return $sum + end($benefits)['capitalBalance'];
                },
                0
            ),
            'meta'  => [
                'description' => 'Somme des capitaux maximum retournés par OpenPK',
                'formula'     => isset($pk['retirementCapital']) ? '= sum(openPK.retirementCapital.projectedRetirementBenefits.capitalBalance)' : '= sum(openPK.projectedRetirementBenefits.capitalBalance)',
                'variables'   => array_reduce(
                    $openPK,
                    function ($vars, $pk) {
                        $pk                                     = json_decode(json_encode($pk), true);
                        $benefits                               = isset($pk['retirementCapital']) ? $pk['retirementCapital']['projectedRetirementBenefits'] : $pk['projectedRetirementBenefits'];
                        $vars['capital ' . (sizeof($vars) + 1)] = end($benefits)['capitalBalance'];
                        return $vars;
                    },
                    []
                ),
            ],
        ];

        $today    = new \DateTime("now");
        $payment  = new \DateTime($this->open3KManager->getPaydate());
        $yearDiff = $payment->diff($today)->format("%y"); //3

        $digest['capitals']['3_pillar'] = [
            'value' => $open3K['solde'] + ($open3K['amount'] * $yearDiff) + ($open3K['amount'] * $yearDiff * $open3K['rate'] / 100),
            'meta'  => [
                'description' => "Formule basée sur les valeurs entrées dans le formulaire Open3K et multiplié par le nombre d'années séparant l'utilisateur de sa retraite",
                'formula'     => '= open3K.solde + (open3K.amount * yearDiff) + (open3K.amount * yearDiff * open3K.rate / 100)',
                'variables'   => [
                    'open3K.solde'    => $open3K['solde'],
                    'open3K.amount'   => $open3K['amount'],
                    'open3K.rate'     => $open3K['rate'],
                    'yearDiff'        => $yearDiff,
                    'today'           => $today->format('d/m/Y'),
                    'date_of_payment' => $payment->format('d/m/Y'),
                ],
            ],
        ];

        $digest['capitals']['vested_benefits'] = [
            'value' => $open3K['vested_benefits'] ?? 0,
            'meta'  => [
                'description' => 'Valeur entrée lors du formulaire du 3ème pilier',
                'formula'     => '= open3K.vested_benefits',
            ],
        ];

        $digest['capitals']['total'] = [
            'value' => array_reduce(
                $digest['capitals'],
                function ($sum, $capital) {
                    return $sum + $capital['value'];
                },
                0
            ),
            'meta'  => [
                'description' => 'Somme des capitaux',
                'formula'     => '= capitals.2_pillar + capitals.3_pillar + capitals.vested_benefits',
            ],
        ];

        return $digest;
    }

    public function manageOpenAKCalculator($openAKResponse)
    {
        // $data = json_decode($openAKResponse->getBody());
        $data = $openAKResponse;

        if (!isset($data['originalPension'])) {
            return;
        }

        $date = current($data['originalPension'])['date'] ?? false;

        if (!$date) {
            return;
        }

        if (!$this->open3KManager->getPaydate()) {
            $this->open3KManager->savePaydate($date);
        }
    }
}
