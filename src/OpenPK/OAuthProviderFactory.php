<?php

namespace Dibs\Api\OpenPk;

use Dibs\Api\OpenPK\OAuthProvider;

class OAuthProviderFactory
{
    protected $clientId;
    protected $clientSecret;
    protected $redirectUri;

    public function __construct($clientId, $clientSecret, $redirectUri, $redirectGrantUri)
    {
        $this->clientId         = $clientId;
        $this->clientSecret     = $clientSecret;
        $this->redirectUri      = $redirectUri;
        $this->redirectGrantUri = $redirectGrantUri;
    }

    public function getProvider($openPKOauthInfo)
    {
        return new OAuthProvider(
            [
            'clientId'                => $this->clientId,
            'clientSecret'            => $this->clientSecret,
            'redirectUri'             => $this->redirectUri,
            'redirectGrantUri'        => $this->redirectGrantUri,
            'urlAuthorize'            => $openPKOauthInfo->authorizeUrl,
            'urlAccessToken'          => $openPKOauthInfo->tokenEndpoint,
            'urlResourceOwnerDetails' => $openPKOauthInfo->policyEndpoint . '/users/me',
            ]
        );
    }
}
