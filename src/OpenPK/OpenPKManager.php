<?php

namespace Dibs\Api\OpenPK;

use Dibs\Api\Data\DataManager;
use Dibs\Api\Exceptions\UnauthorizedException;
use Dibs\Api\OpenPk\OAuthProviderFactory;
use Dibs\Api\User\UserManager;
use GuzzleHttp\ClientInterface as HttpClient;
use League\OAuth2\Client\Token\AccessToken;
use Dibs\Api\Synthesis\SimulationData;

class OpenPKManager
{
    protected $httpClient;
    protected $dataManager;
    protected $userManager;
    protected $oAuthProviderFactory;

    public function __construct(HttpClient $httpClient, DataManager $dataManager, UserManager $userManager, OAuthProviderFactory $oAuthProviderFactory)
    {
        $this->httpClient           = $httpClient;
        $this->dataManager          = $dataManager;
        $this->userManager          = $userManager;
        $this->oAuthProviderFactory = $oAuthProviderFactory;
    }

    public function savePensionFundSelection($ids)
    {
        $this->userManager->appendToPrivateDb('openpk.pensions.selection', $ids);
        $this->userManager->appendToPrivateDb('openpk.policies', null);
    }

    public function getPensionFundSelection()
    {
        return $this->userManager->retrieveFromPrivateDb('openpk.pensions.selection');
    }

    public function getPensionFundGrants()
    {
        $grants = $this->userManager->retrieveFromPrivateDb('openpk.pensions.grants');
        return $grants ? json_decode($grants, true) : [];
    }

    public function getPensionFundTokens()
    {
        $tokens = $this->userManager->retrieveFromPrivateDb('openpk.pensions.consents');
        return $tokens ? json_decode($tokens, true) : [];
    }

    public function getPensionFundUserData()
    {
        return $this->userManager->retrieveFromPrivateDb('openpk.pensions') ?? [];
    }

    public function getRegistry($filtered = false)
    {
        $registry = $this->dataManager->get('openpk::registry');

        if (!$registry) {
            $response = $this->httpClient->request('GET', 'pension-funds');
            $registry = (string) $response->getBody();

            $this->dataManager->set('openpk::registry', $registry, 60 * 30);
        }

        $registry = json_decode($registry);

        if ($filtered) {
            $registry = array_map(
                function ($pension) {
                    unset($pension->openpkServices);
                    unset($pension->openpkStatus);
                    unset($pension->address);

                    return $pension;
                },
                (array) $registry
            );
        }

        $registry = array_combine(array_column($registry, 'id'), $registry);
        return $registry;
    }

    public function getNextConsentRegistryInfo()
    {
        $registry  = $this->getRegistry();
        $selection = $this->getPensionFundSelection();
        $tokens    = $this->getPensionFundTokens();

        $toBeConsented = array_filter(
            $selection,
            function ($id) use ($tokens, $registry) {
                $token = $tokens[$id] ?? null;
                $info  = $registry[$id];

                return !$this->testTokenIsValid($info, $token);
            }
        );

        return sizeof($toBeConsented) > 0 ? $registry[array_shift($toBeConsented)] : null;
    }

    public function getNextGrantRegistryInfo()
    {
        $registry  = $this->getRegistry();
        $selection = $this->getPensionFundSelection();
        $grants    = $this->getPensionFundGrants();
        $tokens    = $this->getPensionFundTokens();

        $toBeGranted = array_filter(
            $selection,
            function ($id) use ($grants, $tokens, $registry) {
                $token = $tokens[$id] ?? null;
                $info  = $registry[$id];

                return !$this->testGrantIsValid($info, $token);
            }
        );

        return sizeof($toBeGranted) > 0 ? $registry[array_shift($toBeGranted)] : null;
    }

    public function getGrantUrl($registryInfo)
    {
        $provider = $this->getProvider($registryInfo);
        $authorizationUrl        = $provider->getGrantUrl($registryInfo);

        return $authorizationUrl;
    }

    public function getAuthorizationUrl($registryInfo)
    {
        $provider = $this->getProvider($registryInfo);

        $authorizationUrl        = $provider->getAuthorizationUrl(['scope' => 'offline_access openid profile email']);
        $_SESSION['oauth2state'] = $provider->getState();

        return $authorizationUrl;
    }

    public function isStateOkay(string $state = null)
    {
        return $state
        && isset($_SESSION['oauth2state'])
            && ($state == $_SESSION['oauth2state']);
    }

    public function getPolicies(SimulationData $simulationData = null)
    {
        $policies = $this->userManager->retrieveFromPrivateDb('openpk.policies');
        if ($simulationData || !$policies) {
            $registry = $this->getRegistry();
            $policies = array_reduce(
                $this->getPensionFundSelection(),
                function ($policies, $pensionId) use ($registry, $simulationData) {

                    if (!isset($registry[$pensionId])) {
                        return $policies;
                    }

                    $pensionInfo = $registry[$pensionId];
                    $accessToken = $this->getValidAccessToken($pensionInfo);

                    $provider = $this->getProvider($pensionInfo);
                    $own      = $provider->getResourceOwner($accessToken);
                    $request  = $provider->getAuthenticatedRequest('GET', $pensionInfo->openpkServices->policyEndpoint . '/users/me', $accessToken);
                    $response = $provider->getResponse($request);
                    $grant     = json_decode($response->getBody()->getContents());


                    if ($simulationData) {
                        $request  =  $provider->getAuthenticatedRequest('POST', $pensionInfo->openpkServices->policyEndpoint . '/policies/' . $grant->policyGrants[0]->policyId . '/salary-change-simulation', $accessToken, [
                          'body' => json_encode($simulationData),
                          'headers' => [
                            'Content-Type' => 'application/json;',
                          ]
                        ]);
                    } else {
                        $request  = $provider->getAuthenticatedRequest('GET', $pensionInfo->openpkServices->policyEndpoint . '/policies/' . $grant->policyGrants[0]->policyId, $accessToken);
                    }
                    $response = $provider->getResponse($request);
                    $policies[$pensionId]     = json_decode($response->getBody()->getContents());

                    return $policies;
                },
                []
            );
            $this->userManager->appendToPrivateDb('openpk.policies', $policies);
        }

        return $policies;
    }

    protected function getValidAccessToken($registryInfo)
    {
        $tokens   = $this->getPensionFundTokens();
        $policyId = $registryInfo->id;

        if (!isset($tokens[$policyId])) {
            throw new UnauthorizedException("Les consentements de la pension " . $policyId . " n'ont pas encore été donnés");
        }

        $accessToken = $this->testTokenIsValid($registryInfo, $tokens[$policyId]);

        if (!$accessToken) {
            unset($tokens[$policyId]);
            $this->userManager->appendToPrivateDb('openpk.pensions.consents', json_encode($tokens));
            throw new UnauthorizedException("Les tokens de la pension " . $policyId . " ne sont plus valides");
        }

        return $accessToken;
    }

    protected function testTokenIsValid($registryInfo, $token)
    {

        if (!$token) {
            return false;
        }

        $accessToken = new AccessToken($token);

        if ($accessToken->hasExpired()) {
            $accessToken = $this->refreshAccessToken($registryInfo, $accessToken);
        }

        return $accessToken;
    }

    protected function testGrantIsValid($registryInfo, $token)
    {

        if (!$token) {
            return false;
        }

        $accessToken = new AccessToken($token);

        if ($accessToken->hasExpired()) {
            $accessToken = $this->refreshAccessToken($registryInfo, $accessToken);
        }

        $provider      = $this->getProvider($registryInfo);
        $resourceOwner = $provider->getResourceOwner($accessToken);
        $resourceOwner = $resourceOwner->toArray();

        return $resourceOwner['personGrant'] ?? false;
    }

    protected function getProvider($registryInfo)
    {
        $openPKInfo = $registryInfo->openpkServices;
        return $this->oAuthProviderFactory->getProvider($openPKInfo);
    }

    protected function refreshAccessToken($registryInfo, AccessToken $accessToken)
    {
        $provider = $this->getProvider($registryInfo);

        try {
            $newAccessToken = $provider->getAccessToken(
                'refresh_token',
                [
                'refresh_token' => $accessToken->getRefreshToken(),
                ]
            );
        } catch (\Exception $e) {
            throw new UnauthorizedException('Impossible de rafraichir le token: ' . $e->getMessage());
        }

        $this->storeAccessToken($registryInfo, $newAccessToken);

        return $newAccessToken;
    }

    public function retrieveAccessToken($registryInfo, string $code)
    {
        $provider = $this->getProvider($registryInfo);

        $accessToken = $provider->getAccessToken(
            'authorization_code',
            [
            'code' => $code,
            ]
        );

        $this->storeAccessToken($registryInfo, $accessToken);
    }

    protected function storeAccessToken($registryInfo, AccessToken $accessToken)
    {
        $tokens                    = $this->getPensionFundTokens();
        $tokens[$registryInfo->id] = $accessToken;
        $this->userManager->appendToPrivateDb('openpk.pensions.consents', json_encode($tokens));
        $this->userManager->appendToPrivateDb('openpk.policies', null);
    }
}
