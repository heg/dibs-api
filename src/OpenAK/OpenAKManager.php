<?php

namespace Dibs\Api\OpenAK;

use Dibs\Api\User\UserManager;
use GuzzleHttp\ClientInterface as HttpClient;
use Dibs\Api\Synthesis\SimulationData;

class OpenAKManager
{
    public function __construct(HttpClient $httpClient, UserManager $userManager)
    {
        $this->httpClient  = $httpClient;
        $this->userManager = $userManager;
    }

    public function test()
    {
        return $this->httpClient->request('GET', '/health');
    }

    public function getPensionCalculatorInfo()
    {
        return [
        'navs'      => $this->userManager->retrieveFromPrivateDb('openak.navs'),
        'birthDate' => $this->userManager->retrieveFromPrivateDb('openak.birthDate'),
        'sex' => $this->userManager->retrieveFromPrivateDb('openak.sex'),
        'relationships' => $this->userManager->retrieveFromPrivateDb('openak.relationships'),
        ];
    }

    public function savePensionCalculatorInfo($navs, $birthDate, $sex, $relationships)
    {
        $this->userManager->appendToPrivateDb('openak.navs', $navs);
        $this->userManager->appendToPrivateDb('openak.birthDate', $birthDate);
        $this->userManager->appendToPrivateDb('openak.sex', $sex);
        $this->userManager->appendToPrivateDb('openak.relationships', $relationships);
        $this->userManager->appendToPrivateDb('openak.calculator', null);
    }

    /**
     * Récupérer les informations de pension
     *
     * @return GuzzleHttp\Psr7\Response Réponse de l'api d'OpenPK
     */
    public function retrievePensionCalculator(SimulationData $simulationData = null)
    {
        $calculator      = $this->userManager->retrieveFromPrivateDb('openak');

        if ($simulationData || !$calculator || !$calculator['calculator']) {
            $calculator = $this->postPensionCalculatorResponse($simulationData);

            $calculator = json_decode($calculator->getBody(), true, 512, JSON_OBJECT_AS_ARRAY);
            if (!$simulationData) {
                $this->userManager->appendToPrivateDb('openak.calculator', $calculator);
            }
        } else {
            $calculator = $calculator['calculator'];
        }

        return $calculator;
    }

    /**
     * Récupérer les informations de pension
     *
     * @return GuzzleHttp\Psr7\Response Réponse de l'api d'OpenPK
     */
    public function postPensionCalculatorResponse(SimulationData $simulationData = null)
    {
        $data       = $this->userManager->retrieveFromPrivateDb('openak');
        $navs       = $data['navs'];
        $birthDate  = $data['birthDate'];
        $sex        = strtoupper($data['sex']);
        $calculator = $data['calculator'];
        $relationships = json_decode($data['relationships']);

        $query = [
        'applicant' =>
        [
          'navs' => $navs,
          'birthDate' => $birthDate,
          // 'deathDate' => '',
          'nationality' => 100,
          'residency' => 100,
          'sex' => $sex,
        ],
        'relationships' => $relationships,
        'insureds' =>
        [
          0 =>
          [
            'navs' => $data['navs'],
            // 'nbYearBeforeAfterAgeRetirement' => -2,
            'futureRate' => $simulationData ? $simulationData->employmentLevel : null,
          ],
        ],
        ];

/*        $query = [
  'applicant' => [
    'navs' => 7560000000019,
    'birthDate' => '1982-02-01',
    'nationality' => 100,
    'residency' => 100,
    'sex' => 'FEMALE',
  ],
  'relationships' => [
    0 => [
      'start' => '2000-11-02',
      'type' => 'SPOUSE',
      'person' => [
        'navs' => 7560000000026,
        'birthDate' => '1983-02-04',
        'nationality' => 100,
        'residency' => 100,
        'sex' => 'MALE',
      ],
      // 'navsParent' => '',
    ],
  ],
  'insureds' => [
    // 0 => [
    //   'navs' => 0,
    //   'nbYearBeforeAfterAgeRetirement' => 0,
    //   'futureRate' => 100,
    // ],
  ],
];*/
            // echo json_encode($query);
            // die();

        return $this->httpClient->request(
            'POST',
            'pension-calculator',
            [
            'json' => $query,
            ]
        );
    }
}
